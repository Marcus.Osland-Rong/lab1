package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(3);
        crossSum(12);
    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 7; i <= n; i+=7){
            System.out.println(i);
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++){
            String mulString = Integer.toString(i) + ":";

            for (int j = 1; j <= n; j++){
                mulString += " " + Integer.toString(j*i);
            }

            System.out.println(mulString);
        }

    }

    public static int crossSum(int num) {
        String numString = Integer.toString(num);

        int sum = 0;

        for (char ch : numString.toCharArray()){
            sum += Character.getNumericValue(ch);
        }

        return sum;
    }

}