package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int longest = 0;

        longest = Math.max(word1.length(), Math.max(word2.length(), word3.length()));

        if (word1.length() ==longest) {
            System.out.println(word1);
        }
        if (word2.length() == longest) {
            System.out.println(word2);
        }
        if (word3.length() == longest) {
            System.out.println(word3);
        }

    }


    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            return (year % 100 != 0) || (year % 400 == 0);
        } else {
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0 ){
            return true;
        }
        return false;
    }

}
