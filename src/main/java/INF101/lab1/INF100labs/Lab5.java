package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(3, 3,4,6,4,3,2,3,4,65,4,3,2,13,3,4,32,67));
        System.out.println(removeThrees(list).toString());

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multipliedArray = new ArrayList<Integer>();

        for (int num : list){
            multipliedArray.add(num*2);
        }

        return multipliedArray;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        list.removeIf(num -> num==3);
        
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqIntegers = new ArrayList<Integer>();

        for (int num : list){
            if (!uniqIntegers.contains(num)){
                uniqIntegers.add(num);
            }
        }

        return uniqIntegers;
        
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i=0; i < a.size(); i++ ){
            a.set(i, a.get(i) + b.get(i) );
        }
    }

}