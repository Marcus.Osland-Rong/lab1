package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int rowSum = 0;
        for (ArrayList<Integer> row : grid) {
            int tempRowSum = 0;
            for (int value : row) {
                tempRowSum += value;
            }
            if (rowSum == 0) {
                rowSum = tempRowSum;
            } else if (rowSum != tempRowSum) {
                return false;
            }
        }

        for (int col = 0; col < grid.get(0).size(); col++) {
            int tempColSum = 0;
            for (ArrayList<Integer> row : grid) {
                tempColSum += row.get(col);
            }
            if (rowSum != tempColSum) {
                return false;
            }
        }

        return true;
    }

}